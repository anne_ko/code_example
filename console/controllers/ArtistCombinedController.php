<?php

namespace console\controllers;

use wsm\Music\Artist\Artist;
use wsm\Music\Artist\ArtistManager;
use wsm\Music\Artist\Manage\ArtistCombined;
use wsm\Music\Artist\Manage\ArtistSplitManager;
use wsm\Music\Artist\Manage\Forms\ArtistSplitForm;
use Yii;
use yii\console\Controller;

class ArtistCombinedController extends Controller
{
    /**
     * консольная команда, для заполнения таблицы со всеми артистами, которе могут быть разделены на несколько артистов
     */
    public function actionFillTable()
    {
        $manager = new ArtistSplitManager(new ArtistSplitForm(), new ArtistManager());
        $artists_query = Artist::find()->select(['name', 'id'])->asArray();
        foreach (ArtistCombined::getJoinersList() as $index => $joiner)
        {
            $artists_query->orWhere(['like', 'name', " $joiner "]);
        }
        $artists = $artists_query->all();
        $artists_to_insert = [];
        foreach ($artists as $index => $artist)
        {
            $data = $manager->fillSplittedArtists($artist['name']);
            $extra_artists = [];
            foreach ($data as $index => $datum)
            {
                if ($index == 0)
                {
                    $to_artist = $data[0]['artist'] ? $data[0]['artist']->id : 0;
                    continue;
                }
                if ($data[0]['artist'])
                {
                    $extra_artists[] = $data[0]['artist']->id;
                }
            }

            $artists_to_insert[] = [$artist['name'], $artist['id'], time(), $to_artist, json_encode($extra_artists)];
        }

        ArtistCombined::getDb()
            ->createCommand()
            ->batchInsert(ArtistCombined::tableName(),
                ['old_name', 'id', 'date_created', 'to_artist', 'extra_artists'],
                $artists_to_insert)
            ->execute();
    }
}