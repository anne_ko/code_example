<?php

namespace console\queue\Artist;

use console\queue\JobQueue;
use Exception;
use wsm\Contribution\Tab\Manage\Rename\Manager;
use wsm\Music\Artist\Artist;
use wsm\Music\Artist\Exception\ArtistCombinedException;
use wsm\Music\Artist\Manage\ArtistCombined;
use wsm\Music\Artist\Manage\ArtistCombinedManager;
use wsm\Music\Recording\Exception\RecordingNotFoundException;
use wsm\Music\Recording\RecordingArtist;
use wsm\Music\Tab\Tab;
use wsm\User\User;
use Yii;
use yii\helpers\ArrayHelper;
use yii\queue\Queue;

class ArtistSplitJob extends JobQueue
{
    public
        $from_artist_id,
        $to_artist_id,
        $user_id;
    /** @var array - [["name":"Zac Brown Band","joiner":" & "], [...], ... ] */
    public $extra_artists;

    public function executeSafe(Queue $queue)
    {
        // todo собственно разделение артистов

        // после success, для админки и лога
        $this->updateArtistCombined();
    }

    private function updateArtistCombined()
    {
        if (!($artist_combined = ArtistCombined::findOne($this->from_artist_id)))
        {
            throw new ArtistCombinedException("artist combined not found for artist id = {$this->from_artist_id}");
        }
        $artist_combined_manager = new ArtistCombinedManager($artist_combined);
        $artist_combined_manager
            ->matchArtist($this->to_artist_id, ArrayHelper::getColumn($this->extra_artists, 'id'))
            ->saveArtist();
    }
}