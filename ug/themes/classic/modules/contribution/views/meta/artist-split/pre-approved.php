<?
use yii\widgets\ActiveForm;
use yii\helpers\Html;
/** * @var \wsm\Music\Artist\Manage\Forms\ArtistSplitForm $form */
?>
    <div class="form-modal">
    <span class="lead">
    Артист <u><b><?= $from_artist_name; ?></b> (From Artist)</u> будет склеен с артистом <u><b><?= $form->to_artist_name; ?></b> (To Artist)</u>
<br> а артист(ы) <b><?= implode(", ", \yii\helpers\ArrayHelper::getColumn($form->extra_artists, 'name')) ?></b> будут добавлены как экстра артисты на все табы <b><?= $from_artist_name; ?></b>
    </span>
    </div>
<? $active_form = ActiveForm::begin([
    'action' => ['/contribution/meta/artist-split/process-split'],
    'method' => 'post',
]); ?>
<?= $active_form->field($form, 'from_artist_id')->textInput(['type' => 'hidden'])->label(false) ?>
<?= $active_form->field($form, 'to_artist_name')->textInput(['type' => 'hidden'])->label(false) ?>
<? foreach ($form->extra_artists as $k => $extra_artist):?>
    <?= $active_form->field($extra_artist, "[{$k}]name")->textInput(['type' => 'hidden'])->label(false) ?>
    <?= $active_form->field($extra_artist, "[{$k}]joiner")->textInput(['type' => 'hidden'])->label(false) ?>
<? endforeach; ?>
    <div class="form-group">
        <?= Html::submitButton('Start', ['class' => 'btn btn-default']) ?>
    </div>
<?php ActiveForm::end(); ?>
    <h3>Существующие альбомы</h3>
Дубли <br>
<? foreach ($split_manager->getDublicateAlbums() as $index => $duplicate_album) : ?>
    <?= $duplicate_album; ?> <br>
<? endforeach; ?>
<?php
foreach ($albums as $artist_id => $albums_data)
{
    foreach ($albums_data as $index => $album_data)
    {
        if ($index == 0)
        {
            if ($form->to_artist_name == $album_data['artist_name'])
            {
                echo "<h4>Albums (To Artist): {$album_data['artist_name']}</h4>";
            }
            else if ($from_artist_name == $album_data['artist_name'])
            {
                echo "<h4>Albums (From Artist): {$album_data['artist_name']}</h4>";
            }
            else
            {
                echo "<h4>Albums (Extra Artist): {$album_data['artist_name']}</h4>";
            }
        }
        echo \yii\helpers\Html::a($album_data['album']->name, ['/contribution/meta/album/update?id=' . $album_data['album']->id]) . '<br>';
    }
}
?>