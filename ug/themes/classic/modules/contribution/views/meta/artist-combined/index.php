<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->pageTitle = $this->title = 'Список всех артистов';
?>
<h2>
    <?= $this->pageTitle; ?>
    <?= $this->render('//modules/contribution/views/meta/common/_create_btn', [
        'name'    => 'artist',
        'routing' => 'artist',
    ]); ?>
</h2>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    //        'filterModel'  => $searchModel,
    'columns'      => [
        'id',
        [
            'attribute' => 'old_name',
            'value'     => function ($model) use ($artist_manager) {
                return Html::a($model->getOldName(), \yii\helpers\Url::toRoute(['update', 'id' => $model->getId()]));
            },
            'format'    => 'raw',
        ],
        'to_artist_id',
        'extra_artist_ids',
        'artist.discogs_id',
        'artist.tabscount',

        [
            'attribute' => 'artist.album',
            'value'     => function ($model) use ($artist_manager) {
                if ($model->artist && $model->artist->album)
                {
                    return count($model->artist->album);
                }
                return '';
            },
            'format'    => 'raw',
        ],
        [
            'attribute' => 'status',
            'value'     => function ($model) use ($artist_manager) {
                return Html::button('Mark Artist as Valid', [
                    'class'   => 'btn-success btn',
                    'onclick' => "changeStatus('ignored', {$model->getId()})",
                ]);
            },
            'format'    => 'raw',
        ],
        [
            'class'    => \yii\grid\ActionColumn::class,
            'template' => '{update}',
        ],
    ],
]); ?>
<script>
    function changeStatus(status, id) {
        $.ajax('/contribution/meta/artist-combined/update-status?id=' + id + '&status=' + status, {
            dataType: "json",
            success: function (data) {
                if (data.result == 'ok') {
                    alert('OK');
                    return;
                }
                alert('Something went wrong.');
            },
            error: function (data) {
                alert('Something went wrong.');
            }
        });
    }
</script>