<?

use yii\widgets\ActiveForm;
use yii\helpers\Html;

/** @var \wsm\Music\Artist\Manage\Forms\ArtistSplitForm $form_split_artists */
/** @var \wsm\Music\Artist\Manage\ArtistCombined $old_artist */

$this->pageTitle = $this->title = 'Редактирование артиста "' . $old_artist->getOldName() . '"';
$this->params['current_breadcrumb'] = \yii\helpers\Html::a('Все артисты ', [\yii\helpers\Url::to('index')]);
$link_options = ['target' => '_blank'];
$old_artist_name = $old_artist->artist ? $old_artist->artist->getName() : $old_artist->getOldName();
?>
    <h2><?= $old_artist_name ?></h2>
<? $form = ActiveForm::begin([
    'action' => ['/contribution/meta/artist-split/pre-approved'],
    'method' => 'post',
]); ?>
    <input type="hidden" name="from_artist_name" value="<?= $old_artist->old_name; ?>">
    <div class="form-group">
        <?= Html::submitButton('Start', ['class' => 'btn btn-default']) ?>
    </div>
<?= $form->field($form_split_artists, 'from_artist_id')->textInput(['type' => 'hidden', 'value' => $old_artist->getId()])->label(false) ?>
    <h4>У артиста <?= $old_artist_name ?> - <?= $old_artist->artist ? count($old_artist->artist->tabs) : 0; ?> табов</h4>
    Albums: <?= count($old_artist->artist->album); ?>
    <br>Albums with discogs_release_id: <?= $artist_manager->getAlbumDiscogsCount($old_artist->artist->album); ?>

    <ul class="">
        <? foreach ($old_artist->artist->tabs as $tab): ?>
            <? if ($tab->isApproved())
            {
                echo \wsm\Music\Artist\Manage\ArtistCombinedManager::getTabUrlHtml($tab);
            } ?>
        <? endforeach; ?>
    </ul>

    <hr>
    <div class="col-md-3 pull-left">
        <h5>Этот артист будет указан как главный для всех табов</h5>
        <?= $form->field($form_split_artists, 'to_artist_name')->input('text', ['id' => "artistsplitform-extra_artist_names", 'class' => "js-artist-splitted form-control", 'data-key' => "0"])->label(false) ?>
        <div class="js-unknown-artist js-unknown-artist0 unknow-artist" style="display: none">
            <div class="alert alert-danger fade in">
                <button type="button" class="close" data-dismiss="alert" aria-hidden>×</button>
                <p>
                    <strong>Unknown artist!</strong> We were not able to find "<span class="js-unknown-artist-name0"></span>" artist's name in our database
                </p>
            </div>
        </div>

        <div class="" style="float: none;">
            <span class="js-info-pending" style="display: none">Ожидайте...</span>
            <h4>У артиста "<span class="js-new-artist-name0"><? if ($form_split_artists->artist): ?>
                        <?= \yii\helpers\Html::a($form_split_artists->artist->getName(), $form_split_artists->artist->getUrl(), $link_options); ?>
                    <? else: ?><?= $form_split_artists->to_artist_name; ?><? endif; ?></span>" есть
                <span class="js-new-artist-tabs-count0"><?= $form_split_artists->artist ? count($form_split_artists->artist->tabs) : 0; ?></span> табов.
            </h4>
            <h5>Эти табы склеятся с теми <?= count($old_artist->artist->tabs) ?> шт, которые есть у <?= $old_artist->artist->getName(); ?></h5>
            <ul class="js-new-artist-tabs0">
                <? if ($form_split_artists->artist && $form_split_artists->artist->tabs): ?>
                    <? foreach ($form_split_artists->artist->tabs as $tab): ?>
                        <li><?= \yii\helpers\Html::a($tab->getName(), $tab->getUrl(), $link_options); ?>: rating <?= number_format($tab->getRating(), 2); ?> (votes <?= $tab->getVotes(); ?>), <?= $tab->getCommentCount(); ?> comments</li>
                    <? endforeach; ?>
                <? endif; ?>
            </ul>
            <hr>
        </div>
    </div>


    <div class="col-md-9 js-extra-artists">
        <? foreach ($form_split_artists->extra_artists as $index => $extra_artist) : ?>
            <?= $this->render('_artist_block', [
                'link_options'   => $link_options,
                'extra_artist'   => $extra_artist,
                'form'           => $form,
                'old_artist'     => $old_artist,
                'index'          => $index + 1,
                'form_index'     => $index,
                'artist_manager' => $artist_manager,
            ]); ?>
        <? endforeach; ?>
    </div>
<?php ActiveForm::end(); ?>
<?

$this->registerJsFile(
    '//' . \Yii::$app->params['img_serv'] . '/js/typeahead' . \Yii::$app->params['statik_cache'] . '.js',
    ['position' => \yii\web\View::POS_END]
);
$this->registerJsFile(
    '//' . \Yii::$app->params['img_serv'] . '/js/artist_combined' . \Yii::$app->params['statik_cache'] . '.js',
    ['position' => \yii\web\View::POS_END]
);
?>