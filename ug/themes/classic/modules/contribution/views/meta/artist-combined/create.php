<div class="form-modal">
    <?=$this->render('_form', [
        'artist'      => $artist,
        'js_response' => true,
    ]); ?>
</div>