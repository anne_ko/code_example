<?
    use yii\helpers\Url;
?>

<form class="form" action="<?=Url::to('/contribution/meta/artist/save'); ?>" method="POST">

    <input type="hidden" name="id" value="<?=$artist->getId(); ?>">

    <? if ($js_response): ?>
        <input type="hidden" name="dont_redirect" value="1">
    <? endif ?>
    <? if (!$artist->getId()): ?>
        <div class="form-group">
            <label>Имя</label>
            <input class="form-control" type="string" maxlength="60" name="name" required="true" value="" placeholder="AOKA">
        </div>
    <? endif; ?>

    <div class="form-group">
        <label>Discogs id</label>
        <input class="form-control" type="string" maxlength="60" name="discogs_id" value="<?=$artist->getDiscogsId(); ?>">
    </div>

    <?=\ug\themes\classic\modules\contribution\views\widgets\GenreWidget::widget(); ?>
    <? if ($artist->genres): ?>
        <table class="table">
            <? foreach($artist->genres as $genre_relation): ?>
                <tr>
                    <td>
                        <?=$genre_relation->genre->getName(); ?>
                    </td>
                    <td>
                        <a href="<?=Url::to(['/contribution/meta/artist/genre-delete', 'id' => $genre_relation->getId()]) ?>" target="_blank">
                            <span class="glyphicon glyphicon-trash"></span>
                        </a>
                    </td>
                </tr>
            <? endforeach; ?>
        </table>
    <? endif; ?>

    <? if ($artist->getId()): ?>
        <? if ($artist->aliases): ?>
            Другие имена для "<?=$artist->getName(); ?>" (<a href="<?=Url::to(['/contribution/meta/alias/index', 'artist_id' => $artist->getId()]) ?>">добавить ещё имена</a>):
            <table class="table">
                <? foreach($artist->aliases as $alias): ?>
                    <tr>
                        <td>
                            <?=$alias->getName(); ?>
                        </td>
                        <td>
                            <a href="<?=Url::to(['/contribution/meta/alias/delete', 'id' => $alias->getId()]) ?>">
                                <span class="glyphicon glyphicon-trash"></span>
                            </a>
                        </td>
                    </tr>
                <? endforeach; ?>
            </table>
        <? else: ?>
            У артиста "<?=$artist->getName(); ?>" нет алиасов. <a href="<?=Url::to(['/contribution/meta/alias/index', 'artist_id' => $artist->getId()]) ?>">Создать</a>?
        <? endif; ?>

    <? endif; ?>
             <br>

    <? if ($artist->members): ?>
        Members:
        <table class="table">
            <? foreach($artist->members as $member): ?>
                <tr>
                    <td>
                        <a href="<?=Url::to(['/contribution/meta/human/update', 'id' => $member->human->getId()]) ?>">
                            <?=$member->human->getFullName(); ?>
                        </a>
                    </td>
                </tr>
            <? endforeach; ?>
        </table>
        <br>
    <? endif; ?>


    <? if ($artist->getUrls()): ?>
        <div>Sites:</div>
        <ul>
            <? foreach($artist->getUrls() as $url): ?>
                <li>
                    <a href="<?=$url; ?>" target="blank">
                        <?=$url; ?>
                    </a>
                </li>
            <? endforeach; ?>
        </ul>
    <? endif; ?>

    <input type="submit" value="Сохранить" class="btn btn-success">

</form>
