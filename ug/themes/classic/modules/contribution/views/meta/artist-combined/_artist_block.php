<?
use wsm\Music\Artist\Manage\Forms\ArtistExtraForm;
/** @var \wsm\Music\Artist\Manage\Forms\ArtistExtraForm $extra_artist */
?>
<div class="js-extra-artist pull-left">
    <h5>Этот артист будет добавлен как extraArtist</h5>

    <section class="form-box--row">
        <div class="form-group col-md-9">
            <?= $form->field($extra_artist, "[{$form_index}]joiner")->dropDownList($artist_manager->getAllArtistJoinFields())->label(false) ?>
            <?= $form->field($extra_artist, "[{$form_index}]name")->input('text', ['data-key'=>"$index", 'id'=>"artistsplitform-extra_artist_names$index", 'class'=>"js-artist-splitted form-control"])->label(false) ?>
            <div class="js-unknown-artist js-unknown-artist<?= $index ?> unknow-artist" style="display: none">
                <div class="alert alert-danger fade in">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden>×</button>
                    <p>
                        <strong>Unknown artist!</strong> We were not able to find "<span class="js-unknown-artist-name<?= $index ?>"></span>" artist's name in our database
                    </p>
                </div>
            </div>
        </div>
        <div class="form-box--cell form-box--cell__no-stretch">
            <section class="input-item input-item__btn-no-label">
                <div class="btn btn-default js-remove">
                    <div class="fa fa-trash-o"></div>
                </div>
            </section>
        </div>
    </section>
    <span class="js-info-pending" style="display: none">Ожидайте...</span>
    <h4>У артиста "<span class="js-new-artist-name<?= $index ?>"><? if ($extra_artist->artist): ?>
                <?= \yii\helpers\Html::a($extra_artist->artist->getName(), $extra_artist->artist->getUrl(), $link_options); ?>
            <? else: ?><?= $extra_artist->name; ?><? endif; ?></span>" есть
        <span class="js-new-artist-tabs-count<?= $index ?>"><?= $extra_artist->artist ? count($extra_artist->artist->tabs) : 0; ?></span> табов.

    </h4>
    <ul class="js-new-artist-tabs<?= $index ?>">
        <? if ($extra_artist->artist && $extra_artist->artist->tabs): ?>
            <? foreach ($extra_artist->artist->tabs as $tab): ?>
                <li><?= \yii\helpers\Html::a($tab->getName(), $tab->getUrl(), $link_options); ?>: rating <?= number_format($tab->getRating(), 2); ?> (votes <?= $tab->getVotes(); ?>), <?= $tab->getCommentCount(); ?> comments</li>
            <? endforeach; ?>
        <? endif; ?>
    </ul>
    <hr>
</div>