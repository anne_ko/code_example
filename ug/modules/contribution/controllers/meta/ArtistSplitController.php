<?php

namespace ug\modules\contribution\controllers\meta;

use wsm\Music\Artist\ArtistManager;
use wsm\Music\Artist\Manage\ArtistSplitManager;
use wsm\Music\Artist\Manage\Forms\ArtistExtraForm;
use wsm\Music\Artist\Manage\Forms\ArtistSplitForm;
use Yii;

class ArtistSplitController extends MetaController
{
    private $artist_manager;

    public function __construct($id, $module, ArtistManager $artist_manager, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->artist_manager = $artist_manager;
    }

    public function behaviors()
    {
        return [
            [
                'class'   => \yii\filters\ContentNegotiator::className(),
                'only'    => ['process-split'],
                'formats' => [
                    '*/*' => \yii\web\Response::FORMAT_JSON,
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['ugOfficeUser'],
                    ],
                ],
            ],
        ];
    }

    public function actionPreApproved()
    {
        $form_name = (new \ReflectionClass(new ArtistExtraForm))->getShortName();
        $form = new ArtistSplitForm(count(Yii::$app->request->post($form_name, [])));

        if ($form->load(Yii::$app->request->post()) && $form->validate())
        {
            $split_manager = (new ArtistSplitManager($form, $this->artist_manager))
                ->fillArtists(false);

            return $this->render('pre-approved', [
                'from_artist_name' => Yii::$app->request->post('from_artist_name'),
                'albums'           => $split_manager->getAlbumsForArtists(),
                'form'             => $form,
                'split_manager'    => $split_manager,
            ]);
        }

        return $form->getFirstErrors() ? array_values($form->getFirstErrors())[0] : 'error';
    }

    public function actionProcessSplit()
    {
        $response = $this->prepareAjaxResponse();
        $form_name = (new \ReflectionClass(new ArtistExtraForm))->getShortName();
        $form = new ArtistSplitForm(count(Yii::$app->request->post($form_name, [])));
        if ($form->load(Yii::$app->request->post()) && $form->validate())
        {
            try
            {
                $split_manager = (new ArtistSplitManager($form, $this->artist_manager))
                    ->fillArtists();
                /**
                 * само разделение оригинального артиста и изменения всех взаимосвязанных сущностей делается через очередь
                 */
                \common\components\queue\QueueManager::getQueue()->push(new \console\queue\Artist\ArtistSplitJob([
                    'from_artist_id' => $split_manager->getFromArtistId(),
                    'to_artist_id'   => $split_manager->getToArtistId(),
                    'user_id'        => Yii::$app->user->getUserId(),
                    'extra_artists'  => $split_manager->getExtraArtists(true),
                ]));
                $response['result'] = self::AJAX_SUCCESS;
            }
            catch (\Exception $exc)
            {
                $response['info'] = $exc->getMessage();
            }
        }

        return $response;
    }
}
