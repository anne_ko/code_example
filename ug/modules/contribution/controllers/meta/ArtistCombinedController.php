<?php

namespace ug\modules\contribution\controllers\meta;

use wsm\Music\Artist\Artist;
use wsm\Music\Artist\Manage\ArtistSplitManager;
use wsm\Music\Artist\Manage\Forms\ArtistSplitForm;
use Yii;
use wsm\Music\Artist\Manage\ArtistCombined;
use wsm\Music\Artist\Manage\ArtistCombinedManager;
use yii\web\NotFoundHttpException;

class ArtistCombinedController extends MetaController
{
    private $artist_combined_manager;
    private $artist_split_manager;

    public function __construct($id, $module, ArtistCombinedManager $artist_combined_manager, ArtistSplitManager $artist_split_manager, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->artist_combined_manager = $artist_combined_manager;
        $this->artist_split_manager = $artist_split_manager;
    }

    public function behaviors()
    {
        return [
            [
                'class'   => \yii\filters\ContentNegotiator::className(),
                'only'    => ['update-status', 'get-artist-tabs'],
                'formats' => [
                    '*/*' => \yii\web\Response::FORMAT_JSON,
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['ugOfficeUser'],
                    ],
                ],
            ],
        ];
    }

    public function actionGetArtistTabs($artist_name)
    {
        $artist = Artist::find()->filterByName($artist_name)->limit(1)->one();
        if ($artist && $artist->tabs)
        {
            $tabs = [];
            foreach ($artist->tabs as $tab)
            {
                if ($tab->isApproved())
                {
                    $tabs[] = \wsm\Music\Artist\Manage\ArtistCombinedManager::getTabUrlHtml($tab, false);
                }
            }

            return [
                'result' => self::AJAX_SUCCESS,
                'info'   => [
                    'count' => count($artist->tabs),
                    'tabs'  => $tabs,
                ],
            ];
        }
    }

    public function actionUpdateStatus($id, $status)
    {
        $artist = $this->findModel($id);
        $artist->setStatus($status);

        if ($artist->save())
        {
            return ['result' => self::AJAX_SUCCESS];
        }

        return ['result' => self::AJAX_ERROR];
    }

    public function actionUpdate($id)
    {
        $artist = $this->findModel($id);
        $splitted_artists = $this->artist_split_manager->fillSplittedArtists($artist->getOldName(), true);
        $form_split_artists = new ArtistSplitForm(count($this->artist_split_manager->getExtraArtists()));
        $form_split_artists->load($splitted_artists);

        return $this->render('update', [
            'old_artist'         => $artist,
            'artist_manager'     => $this->artist_combined_manager,
            'form_split_artists' => $form_split_artists,
        ]);
    }

    public function actionIndex($status = ArtistCombined::STATUS_NEW)
    {
        $query = ArtistCombined::find()
            ->alias('c')
            ->useReplica()
            ->where(['c.status' => $status])
            ->joinWith('artist artist');

        return $this->render('index', [
            'dataProvider'   => $this->artist_combined_manager->getDataProvider($query),
            'artist_manager' => $this->artist_combined_manager,
        ]);
    }

    private function findModel($id)
    {
        if (!($artist = ArtistCombined::find()
            ->alias('c')
            ->where(['c.id' => $id])
            ->joinWith(['artist', 'news'], true)
            ->one()))
        {
            throw new NotFoundHttpException;
        }

        return $artist;
    }

}