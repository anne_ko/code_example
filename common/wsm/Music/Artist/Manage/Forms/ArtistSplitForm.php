<?php

namespace wsm\Music\Artist\Manage\Forms;

use wsm\Music\Artist\Artist;
use elisdn\compositeForm\CompositeForm;

class ArtistSplitForm extends CompositeForm
{
    public $from_artist_id;
    public $to_artist_name;
    /** @var Artist */
    public $artist;

    public function __construct($artistsCount = 0, $config = [])
    {
        $this->extra_artists = $artistsCount ? array_map(function () {
            return new ArtistExtraForm();
        }, range(1, $artistsCount)) : [];
        parent::__construct($config);
    }

    protected function internalForms()
    {
        return ['extra_artists'];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['to_artist_name', 'from_artist_id',], 'required'],
            ['from_artist_id', 'integer'],
            [['to_artist_name',], 'trim'],
            [['to_artist_name',], 'string', 'min' => 2,],
        ];
    }
}