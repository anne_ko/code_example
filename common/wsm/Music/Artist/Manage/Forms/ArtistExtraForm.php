<?php

namespace wsm\Music\Artist\Manage\Forms;

use wsm\Music\Artist\Artist;
use wsm\User\Collection\TabDraft\TabDraft;
use yii\base\Model;

class ArtistExtraForm extends Model
{
    public $name;
    public $joiner;
    /** @var Artist */
    public $artist;
    public $id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'joiner',], 'required'],
            [['name', 'joiner'], 'filter', 'filter' => 'trim',],
            [['name',], 'string', 'min' => 2,],
            [['joiner',], 'string', 'min' => 1,],
            [['name', 'joiner'], 'itemsNotEmpty',],
            ['joiner', 'addSpaces',],
        ];
    }

    public function addSpaces($attribute, $params, $validator)
    {
        $this->$attribute = ' ' . trim($this->$attribute) . ' ';
    }

    public function itemsNotEmpty($attribute, $params, $validator)
    {
        if (!strlen($this->$attribute) || in_array($this->$attribute, TabDraft::EMPTY_VALUES))
        {
            $this->addError($attribute, "$attribute can not be empty");

            return;
        }
    }

}