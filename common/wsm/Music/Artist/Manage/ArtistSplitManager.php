<?php

namespace wsm\Music\Artist\Manage;

use wsm\Music\Album\Album;
use wsm\Music\Artist\Artist;
use wsm\Music\Artist\ArtistManager;
use wsm\Music\Artist\Manage\Forms\ArtistExtraForm;
use wsm\Music\Artist\Manage\Forms\ArtistSplitForm;
use yii\helpers\ArrayHelper;

class ArtistSplitManager
{
    private $from_artist_id;
    private $to_artist_name;

    private $extra_artists;
    private $artist_manager;
    private $duplicate_albums;

    private $to_artist_id;

    /**
     * ArtistSplitManager constructor.
     * @param ArtistSplitForm $form - содержит имя артиста, имя джойнера, id существующего в базе сопоставленного ему артиста (если нашелся)
     * @param ArtistManager $artist_manager - для поиска/создания артистов
     */
    public function __construct(ArtistSplitForm $form, ArtistManager $artist_manager)
    {
        $this->from_artist_id = $form->from_artist_id;
        $this->to_artist_name = $form->to_artist_name;
        $this->extra_artists = $form->extra_artists;

        $this->artist_manager = $artist_manager;
    }

    public function fillToArtist($create)
    {
        if ($create)
        {
            $to_artist = $this->artist_manager->getOrCreateArtistByName($this->to_artist_name);
        }
        else
        {
            $to_artist = $this->artist_manager->getArtistByName($this->to_artist_name);
        }
        $this->to_artist_id = $to_artist ? $to_artist->getId() : 0;
    }

    /**
     * Находит / создает всех артистов, на которых будет разделен combined artist, по именам
     * Заполняет свои аттрибуты id данных артистов
     * @param bool $create
     * @return $this
     */
    public function fillArtists($create = true)
    {
        $this->fillToArtist($create);
        $this->fillExtraArtists($create);

        return $this;
    }

    public function getAlbumsForArtists()
    {
        $albums = Album::find()
            ->alias('album')
            ->where(['in', 'album.artist_id', array_merge([$this->from_artist_id, $this->to_artist_id], $this->getExtraArtistIds())])
            ->with('artist')
            ->orderBy('name')
            ->all();;
        $this->duplicate_albums = array_keys(array_filter(array_count_values(ArrayHelper::getColumn($albums, 'name')), function ($item) {
            return !(1 === $item);
        }));

        $result = [];
        foreach ($albums as $index => $album)
        {
            $result[$album->artist->id][] = [
                'artist_name' => $album->artist->name,
                'album'       => $album,
            ];
        }

        return ($result);
    }

    public function fillExtraArtists($create = true)
    {
        foreach ($this->extra_artists as $index => &$_extra_artist)
        {
            if ($create)
            {
                $extra_artist = $this->artist_manager->getOrCreateArtistByName($_extra_artist['name']);
            }
            else
            {
                $extra_artist = $this->artist_manager->getArtistByName($_extra_artist['name']);
            }
            $_extra_artist['id'] = $extra_artist ? $extra_artist->getId() : 0;
        }

    }

    /**
     * Разбивает имя старого артиста на массив из имен новых артистов, соответствующих им UG артистов и возможных joiner'ов между ними
     * @param $artist_old_name
     * @param bool $with_form_names
     * @return array
     */
    public function fillSplittedArtists($artist_old_name, $with_form_names = false)
    {
        $result = [];
        $artist_name = ($artist_old_name);
        $preg_str = '=';
        foreach (ArtistCombined::getJoinersList() as $index => $joiner)
        {
            $preg_str .= '( ' . preg_quote($joiner) . ' )|';
        }
        $preg_str = substr($preg_str, 0, -1) . ']{1}=i';

        $splitted_artists = preg_split($preg_str, $artist_name);
        $joiners = [];
        $str = '';
        foreach ($splitted_artists as $index => $splitted_artist)
        {
            $str .= $splitted_artist . ' ';

            if ($join = stristr(substr($artist_name, strlen($str)), ' ', true))
            {
                $joiners[$index + 1] = strtolower($join);
                $str .= $join . ' ';
            }
        }
        if ($with_form_names)
        {
            return $this->fillResultsForForm($splitted_artists, $joiners);
        }
        foreach ($splitted_artists as $i => $_artist)
        {
            $result[] = [
                'name'   => trim($_artist),
                'joiner' => isset($joiners[$i]) ? $joiners[$i] : '',
                'artist' => $this->findArtistByName($_artist),
            ];
        }

        return $result;
    }

    private function fillResultsForForm($splitted_artists, $joiners)
    {
        $to_artist_form_name = (new ArtistSplitForm)->formName();
        $extra_artist_form_name = (new ArtistExtraForm)->formName();
        $result = [];
        foreach ($splitted_artists as $i => $_artist)
        {
            $artist = $this->findArtistByName($_artist);
            if ($i === 0)
            {
                $result[$to_artist_form_name] = [
                    'from_artist_id' => $artist ? $artist : 0,
                    'to_artist_name' => trim($_artist),
                    'artist'         => $artist,
                ];
                continue;
            }
            $extra_artist_data = [
                'name'   => trim($_artist),
                'joiner' => isset($joiners[$i]) ? $joiners[$i] : '',
                'artist' => $artist,
            ];
            $this->extra_artists[] = $extra_artist_data;
            $result[$extra_artist_form_name][] = $extra_artist_data;
        }

        return $result;
    }

    public function getFromArtistId()
    {
        return $this->from_artist_id;
    }

    public function getDublicateAlbums()
    {
        return $this->duplicate_albums;
    }

    public function getExtraArtistIds()
    {
        return (ArrayHelper::getColumn($this->extra_artists, 'id'));
    }

    public function getExtraArtists($is_array = false)
    {
        if ($is_array)
        {
            $result = [];
            foreach ($this->extra_artists as $index => $extra_artist)
            {
                $result[] = [
                    'name'   => $extra_artist->name,
                    'joiner' => $extra_artist->joiner,
                    'id'     => $extra_artist->id,
                ];
            }

            return $result;
        }

        return $this->extra_artists;
    }

    public function getExtraArtistJoiners()
    {
        return (ArrayHelper::getColumn($this->extra_artists, 'joiner'));
    }

    public function getToArtistId()
    {
        return $this->to_artist_id;
    }

    /**
     * @param $name
     * @return array|\common\components\db\ActiveRecord|null|Artist|\yii\db\ActiveRecord
     */
    private function findArtistByName($name)
    {
        $name = trim($name);
        $artist = Artist::find()
            ->where('artists.name = binary :name', [':name' => $name])
            ->orderBy(['tabscount' => SORT_DESC])
            ->joinWith(['album'])
            ->one();

        if (!$artist)
        {
            $artist = Artist::find()
                ->where(['artists.name' => $name])
                ->orderBy(['tabscount' => SORT_DESC])
                ->joinWith('album')
                ->one();
        }

        return $artist;
    }
}