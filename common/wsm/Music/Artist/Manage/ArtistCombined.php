<?php

namespace wsm\Music\Artist\Manage;

use wsm\Article\News\News;
use wsm\Music\Artist\Artist;
use Yii;

/**
 * This is the model class for table "artist_combined".
 *
 * @property integer $id
 * @property string $old_name
 * @property integer $to_artist_id
 * @property string $extra_artist_ids
 * @property string $status
 * @property integer $date_created
 */
class ArtistCombined extends \common\components\db\Main
{
    public const STATUS_NEW     = 'new';
    public const STATUS_IGNORED = 'ignored';
    public const STATUS_MATCHED = 'matched';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'artist_combined';
    }

    public function match($to_artist_id, $extra_artist_ids)
    {
        return $this
            ->setStatus(self::STATUS_MATCHED)
            ->setArtistsMatched($to_artist_id, $extra_artist_ids);
    }

    public static function getJoinersList()
    {
        return ['&', 'and', 'feat.', 'feat', 'ft.', 'ft', 'featuring', 'with', 'with/', 'w/', 'W/', '+', 'x', 'vs.', 'vs', ',', '/', 'meet', 'meets'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'string'],
            ['status', 'in', 'range' => self::getStatusList()],
            [['date_created', 'to_artist_id'], 'integer'],
            [['old_name'], 'string', 'max' => 255],
        ];
    }

    public function getStatusList()
    {
        return [self::STATUS_MATCHED, self::STATUS_IGNORED, self::STATUS_NEW];
    }

    public function getNews()
    {
        return $this->hasMany(News::className(), ['owner_id' => 'id']);
    }

    public function getArtist()
    {
        return $this->hasOne(Artist::className(), ['id' => 'id']);
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getOldName()
    {
        return $this->old_name;
    }

    public function setOldName($old_name)
    {
        $this->old_name = $old_name;

        return $this;
    }

    public function getToArtistId()
    {
        return $this->to_artist_id;
    }

    public function setToArtistId($to_artist_id)
    {
        $this->to_artist_id = $to_artist_id;

        return $this;
    }

    public function getExtraArtistIds()
    {
        return $this->extra_artist_ids;
    }

    public function setExtraArtistIds($extra_artist_ids)
    {
        $this->extra_artist_ids = $extra_artist_ids;

        return $this;
    }

    public function setArtistsMatched($to_artist_id, $extra_artist_ids)
    {
        $this->to_artist_id = $to_artist_id;
        $this->extra_artist_ids = json_encode($extra_artist_ids);

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    public function getDateCreated()
    {
        return $this->date_created;
    }

    public function setDateCreated($date_created)
    {
        $this->date_created = $date_created;

        return $this;
    }

}
