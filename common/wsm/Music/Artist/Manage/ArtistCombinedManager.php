<?php

namespace wsm\Music\Artist\Manage;

use wsm\Music\Artist\Exception\ArtistCombinedException;
use yii\data\ActiveDataProvider;

/**
 * Class ArtistCombinedManager - работа с моделями ArtistCombined
 * @package wsm\Music\Artist\Manage
 */
class ArtistCombinedManager
{
    /** @var ArtistCombined */
    private $artist_combined;

    public function __construct($artist_combined = null)
    {
        $this->artist_combined = $artist_combined;
    }

    /** После матчинга артистов, поставить статус и сохранить сматченных артистов
     * @param $to_artist_id
     * @param $extra_artist_ids
     * @return $this
     */
    public function matchArtist($to_artist_id, $extra_artist_ids)
    {
        if ($this->artist_combined)
        {
            $this->artist_combined->match($to_artist_id, $extra_artist_ids);
        }

        return $this;
    }

    public function saveArtist()
    {
        if ($this->artist_combined)
        {
            if (!$this->artist_combined->save())
            {
                throw new ArtistCombinedException("artist combined was not save, id = {$this->artist_combined->id}". json_encode($this->artist_combined->errors));
            };
        }

        return $this;
    }

    /** Расширенный список возможных joiner'ов
     * @return array
     */
    public function getAllArtistJoinFields()
    {
        return array_merge(['__empty__' => ''], array_combine(ArtistCombined::getJoinersList(), ArtistCombined::getJoinersList()));
    }

    public function getDataProvider($query)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->setSort([
            'enableMultiSort' => true,
            'attributes'      => [
                'artist.discogs_id' => [
                    'asc'   => ['artist.discogs_id' => SORT_ASC],
                    'desc'  => ['artist.discogs_id' => SORT_DESC],
                    'label' => 'discogs_id',
                ],
                'artist.tabscount'  => [
                    'asc'   => ['artist.tabscount' => SORT_ASC],
                    'desc'  => ['artist.tabscount' => SORT_DESC],
                    'label' => 'tabscount',
                ],
                'old_name',
                'id',
            ],
        ]);

        return $dataProvider;
    }

    public function getAlbumDiscogsCount($albums)
    {
        $cnt = 0;
        foreach ($albums as $index => $album)
        {
            if ($album->discogs_release_id != 0)
            {
                $cnt++;
            }
        }

        return $cnt;
    }

    public static function getStatusHtml($status)
    {
        switch ($status)
        {
            case ArtistCombined::STATUS_NEW:
                return "<span class=\"btn btn-warning pull-right\">$status</span>";
            case ArtistCombined::STATUS_IGNORED:
                return "<span class=\"btn btn-danger pull-right\">$status</span>";
            case ArtistCombined::STATUS_MATCHED:
                return "<span class=\"btn btn-success pull-right\">$status</span>";
        }
    }

    public static function getTabUrlHtml($tab, $li = true)
    {
        return ($li ? "<li>" : '') .
               \yii\helpers\Html::a(
                   $tab->getName(),
                   $tab->getUrl(),
                   ['target' => '_blank'])
               . ($li ? "</li>" : '');
    }

}