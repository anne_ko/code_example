<?php

namespace wsm\Music\Artist\Exception;

class ArtistDataValidationException extends \wsm\Common\BaseException
{
}