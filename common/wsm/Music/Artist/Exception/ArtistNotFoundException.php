<?php

namespace wsm\Music\Artist\Exception;

class ArtistNotFoundException extends \wsm\Common\BaseException
{
}